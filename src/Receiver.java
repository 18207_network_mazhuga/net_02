import java.io.*;
import java.net.*;

public class Receiver {
    final private int BUFFER_SIZE = 1024 * 200;

    private int port;

    public Receiver(int port) {
        this.port = port;
    }

    public void startFileReceivingServer() {
        try (ServerSocket serverSocket = new ServerSocket(port)
        ) {
            while (true) {
                Socket socket = serverSocket.accept();

                System.out.println("\nConnected to " + socket.getInetAddress());

                socket.setSoTimeout(3000);
                FileReceiveThread thread = new FileReceiveThread(socket);
                thread.run();
            }
        } catch (IOException e) {
            System.err.println("An error has occurred. " + e.getMessage());
        }
    }

    private class FileReceiveThread extends Thread {
        Socket socket;

        public FileReceiveThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try (MyDataOutputStream outputStream = new MyDataOutputStream(socket.getOutputStream());
                 MyDataInputStream inputStream = new MyDataInputStream(socket.getInputStream())) {

                int fileNameLength;
                String fileName;
                long fileSize;

                fileNameLength = inputStream.readInt();
                System.out.println("Name length received: " + fileNameLength);
                fileName = inputStream.readString(fileNameLength);
                System.out.println("Name received: " + fileName);
                fileSize = inputStream.readLong();
                System.out.println("Size received: " + fileSize);

                File folder = new File("uploads");
                folder.mkdir();
                try (FileOutputStream fileStream = new FileOutputStream("uploads/" + fileName)) {
                    byte[] buffer = new byte[BUFFER_SIZE];

                    long totalSent = 0;
                    int lastSent = 0;
                    long averageSpeed = 0;
                    long currentSpeed = 0;

                    long startTime = System.currentTimeMillis();
                    long lastTime;

                    boolean isSending = true;

                    System.out.println("Download started");
                    while (totalSent < fileSize && isSending) {
                        lastTime = System.currentTimeMillis();

                        int length = 0;
                        while (System.currentTimeMillis() - lastTime < 3000) {
                            try {
                                //System.out.println("Started reading");
                                length = inputStream.read(buffer);
                                //System.out.println("Read " + length + " bytes");
                                if (length == -1) {
                                    isSending = false;
                                    break;
                                }

                                fileStream.write(buffer, 0, length);

                                lastSent += length;
                            } catch (SocketTimeoutException e) {
                                System.out.println("Timeout reached");
                            }
                        }

                        long time = System.currentTimeMillis() - lastTime;
                        if (time == 0)
                            time = 1;

                        totalSent += lastSent;
                        //System.out.println("current received " + lastSent + " bytes");
                        //System.out.println("total received " + totalSent + " bytes");
                        currentSpeed = lastSent / time * 1000;
                        averageSpeed = totalSent / (System.currentTimeMillis() - startTime) * 1000;
                        lastSent = 0;

                        System.out.println("Current speed (" + socket.getInetAddress() + "): " + currentSpeed / 1024 + " KB/s");
                        System.out.println("Average speed (" + socket.getInetAddress() + "): " + averageSpeed / 1024 + " KB/s");
                        System.out.println("Progress (" + socket.getInetAddress() + "): " + (float) totalSent / fileSize * 100 + "%");
                        System.out.println();
                    }

                    if (totalSent == fileSize) {
                        outputStream.writeString("success");

                        System.out.println("Sent 'success'");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                System.out.println("Transmission " + socket.getInetAddress() + " completed successfully");
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        int port = Integer.parseInt(args[0]);
        Receiver receiver = new Receiver(port);

        try {
            System.out.println("Started on address " + InetAddress.getLocalHost() + " and port " + port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        receiver.startFileReceivingServer();
    }
}
