import java.io.IOException;
import java.io.OutputStream;

public class MyDataOutputStream extends OutputStream {
    private OutputStream sourceStream;

    public MyDataOutputStream(OutputStream source) {
        sourceStream = source;
    }

    @Override
    public void write(int b) throws IOException {
        sourceStream.write(b);
    }

    public void writeInt(int number) throws IOException {
        write(number >>> 24);
        write(number >>> 16);
        write(number >>> 8);
        write(number >>> 0);
    }

    public void writeLong(long number) throws IOException {
        write((byte) (number >>> 56));
        write((byte) (number >>> 48));
        write((byte) (number >>> 40));
        write((byte) (number >>> 32));
        write((byte) (number >>> 24));
        write((byte) (number >>> 16));
        write((byte) (number >>> 8));
        write((byte) (number >>> 0));
    }

    public void writeString(String string) throws IOException {
        write(string.getBytes());
    }
}
