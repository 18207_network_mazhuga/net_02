import java.io.IOException;
import java.io.InputStream;

public class MyDataInputStream extends InputStream {
    private InputStream sourceStream;

    public MyDataInputStream(InputStream source) {
        sourceStream = source;
    }

    @Override
    public int read() throws IOException {
        return sourceStream.read();
    }

    public int readInt() throws IOException {
        int b1 = read();
        int b2 = read();
        int b3 = read();
        int b4 = read();

        return ((b1 << 24) + (b2 << 16) + (b3 << 8) + b4);
    }

    public long readLong() throws IOException {
        long b1 = read();
        long b2 = read();
        long b3 = read();
        long b4 = read();
        long b5 = read();
        long b6 = read();
        long b7 = read();
        long b8 = read();

        return ((b1 << 56) +
                ((b2 & 255) << 48) +
                ((b3 & 255) << 40) +
                ((b4 & 255) << 32) +
                ((b5 & 255) << 24) +
                ((b6 & 255) << 16) +
                ((b7 & 255) << 8) +
                ((b8 & 255) << 0));
    }

    public String readString(int length) throws IOException {
        byte[] buffer = new byte[length];

        int read = 0;
        while (read != length) {
            read += sourceStream.read(buffer, read, length - read);
        }

        return new String(buffer);
    }
}
