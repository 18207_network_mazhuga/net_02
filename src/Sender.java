import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Sender {
    private InetAddress address;
    private int port;

    final private int BUFFER_SIZE = 1024 * 1024 * 10;
    final private int FINAL_MSG_SIZE = 7;

    public Sender(InetAddress address, int port) {
        this.address = address;
        this.port = port;
    }

    public void sendFile(File file) {
        try (Socket socket = new Socket(address, port);
             MyDataOutputStream outputStream = new MyDataOutputStream(socket.getOutputStream());
             MyDataInputStream inputStream = new MyDataInputStream(socket.getInputStream());
             InputStream fileStream = new FileInputStream(file)) {

            outputStream.writeInt(file.getName().length());
            System.out.println("Name length sent");
            outputStream.writeString(file.getName());
            System.out.println("Name sent");
            outputStream.writeLong(file.length());
            System.out.println("Size sent");

            byte[] buffer = new byte[BUFFER_SIZE];

            System.out.println("Upload started");
            int length;
            while ((length = fileStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, length);
                //outputStream.flush();
                System.out.println("Sent " + length + " bytes");
            }

            String serverMessage = inputStream.readString(FINAL_MSG_SIZE);
            System.out.println("Received message: " + serverMessage);
            if (serverMessage.equals("success"))
                System.out.println("Transmission has competed successfully");
            else
                System.out.println("Transmission has failed");
        } catch (IOException e) {
            System.err.println("An error has occurred. " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        InetAddress address;
        String fileName = args[0];

        final int fileNameMaxSize = 4096;
        final long fileMaxSize = 1024 * 1024 * 1024 * 1024L;

        if (fileName.getBytes().length > fileNameMaxSize) {
            System.err.println("File name is too long");
            return;
        }

        File file = new File(fileName);
        if (file.length() > fileMaxSize) {
            System.err.println("File is too big");
            return;
        }

        try {
            address = InetAddress.getByName(args[1]);
        } catch (UnknownHostException e) {
            System.err.println("Invalid address");
            return;
        }
        int port = Integer.parseInt(args[2]);

        Sender sender = new Sender(address, port);
        sender.sendFile(file);
    }
}
